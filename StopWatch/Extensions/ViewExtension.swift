//
//  ViewExtension.swift
//  StopWatch
//
//  Created by Alexy Ibrahim on 1/25/23.
//

import Foundation
import UIKit

public extension UIView {
    func makeCircular() {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.bounds.height / 2
    }
}
