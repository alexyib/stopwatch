//
//  ViewController.swift
//  StopWatch
//
//  Created by Alexy Ibrahim on 1/18/23.
//

import UIKit
import Combine

class ViewController: UIViewController {
    let viewModel = ViewModel()
    
    @IBOutlet private var label_timer: UILabel!
    @IBOutlet private var button_startStop: UIButton!
    private var observers: [AnyCancellable] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.button_startStop.makeCircular()
    }
    
    func setup() {
        self.title = "Stopwatch"
        
        self.viewModel.timeUpdated.sink { [weak self] value in
            guard let self = self else { return }
            self.refreshTime(time: value)
        }.store(in: &self.observers)
        
        self.viewModel.$timerState.sink { value in
            switch value {
            case .started:
                self.button_startStop.setTitle("Stop", for: .normal)
                self.button_startStop.backgroundColor = .systemRed
                self.button_startStop.setTitleColor(.white, for: .normal)
            case .stopped:
                self.button_startStop.setTitle("Start", for: .normal)
                self.button_startStop.backgroundColor = .systemBlue
                self.button_startStop.setTitleColor(.white, for: .normal)
            case .none:
                break
            }
        }.store(in: &self.observers)
        
        self.label_timer.font = .monospacedSystemFont(ofSize: 22, weight: .regular)
        self.button_startStop.titleLabel?.font = .monospacedSystemFont(ofSize: 30, weight: .bold)
        self.resetTimerLabelUI()
    }
}

// MARK: - Actions
extension ViewController {
    @IBAction func buttonPressed(_ sender: UIButton) {
        switch self.viewModel.timerState {
        case .started:
            self.stopTimer()
        case .stopped:
            self.startTimer()
        case .none:
            break
        }
    }
}

// MARK: - Methods
extension ViewController {
    func startTimer() {
        self.resetTimerLabelUI()
        self.viewModel.startTimer()
    }
    
    func stopTimer() {
        self.viewModel.stopTimer()
    }
    
    func refreshTime(time: Double) {
        self.label_timer.text = self.viewModel.displayTime(time)
    }

    func resetTimerLabelUI() {
        self.label_timer.text = ""
    }
}

